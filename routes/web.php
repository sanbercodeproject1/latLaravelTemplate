<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'pageHome']);
Route::get('/register', [AuthController::class, 'pageRegister']);
Route::post('/welcome', [AuthController::class, 'pageWelcome']);

Route::get('/tables', function() {
  return view('Page.tables');
});
Route::get('/data-tables', function() {
  return view('Page.data-tables');
});
