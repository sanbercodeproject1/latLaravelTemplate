@extends('Layout.master')

@section('tittle')
  Dashboard Page
@endsection

@section('card')
  CARD
@endsection

@section('content')
  <form action="/welcome" method="POST">
    @csrf
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <label>First Name:</label> <br> <br>
    <input type="text" name="first-name"> <br> <br>
    <label>Last Name:</label> <br> <br>
    <input type="text" name="last-name"> <br> <br>
    <label>Gender:</label> <br> <br>
    <input type="radio" name="jenis-kelamin" id="male">
    <label for="male">Male</label> <br>
    <input type="radio" name="jenis-kelamin" id="female">
    <label for="female">Female</label> <br>
    <input type="radio" name="jenis-kelamin" id="other">
    <label for="other">Other</label> <br> <br>
    <label>Nasionality:</label> <br> <br>
    <select name="nasinonality">
      <option value="indonesia">Indonesian</option>
      <option value="singapura">Singapore</option>
      <option value="malaysia">Malaysian</option>
      <option value="australia">Australian</option>
    </select> <br> <br>
    <label>Language Spoken:</label> <br> <br>
    <input type="checkbox" id="indo">
    <label for="indo">Bahasa Indonesia</label> <br>
    <input type="checkbox" id="english">
    <label for="english">English</label> <br>
    <input type="checkbox" id="others">
    <label for="others">Other</label> <br> <br>
    <label>Bio:</label> <br> <br>
    <textarea name="alamat" cols="30" rows="10"></textarea> <br>
    <input type="submit" name="sign-up" value="Sign Up">
  </form>

  @endsection